-- Mock Data For Tests

INSERT INTO users (email, password, role, last_name, first_name, major)
VALUES ('teacher@stevenscollege.edu', 'qwerty', 'teacher', 'Fedjona', 'Zachiberto', 'CSET'),
       ('student@stevenscollege.edu', 'asdfgh', 'student', 'Lueklee', 'Kevstice', 'CSET');
